package francois.rabanel.tp7.services;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.HashMap;

import francois.rabanel.tp7.R;
import francois.rabanel.tp7.activities.MainActivity;
import francois.rabanel.tp7.api.Station;

public class StationService extends Service {
    subscribeStationService service;
    Station newStation;
    private String bikesAvailable, docksAvailable, stationId;
    private String TAG = "StationService";
    public static HashMap<String, Station> hmapStations;

    static {
        hmapStations = new HashMap<>();
    }

    public StationService() {
    }

    private class subscribeStationService extends Thread {
        public void run() {
            Log.d(TAG, "Thread running");
            while (!isInterrupted()) {
                try {
                    // Pull & compare each 30 secondes to avoid to DDOS API.

                    // Refresh datas.
                    MainActivity.loadStations(hmapStations);
                    MainActivity.loadCapacity(hmapStations);

                    // Get the brand new informations about our station.
                    newStation = hmapStations.get(stationId);

                    // Keep the numbers we are interested in.
                    String newBikesAvailable = newStation.getNumBikesAvailable();
                    String newDocksAvailable = newStation.getNumDocksAvailable();

                    Log.d(TAG, "newBikesAvailable: " + newBikesAvailable);
                    Log.d(TAG, "newDocksAvailable: " + newDocksAvailable);

                    // Compare them with the old ones.
                    if (
                            bikesAvailable.equals(newBikesAvailable) &&
                                    docksAvailable.equals(newDocksAvailable)
                    ) {
                        // If they are not differents, do nothing.
                        Log.d(TAG, "Nothing to do. Numbers are unchanged.");
                    } else {
                        // If they are differents, there is an update to send to our user
                        // with a notification.
                        Log.d(TAG, "Brand new things available with your favorite station.");

                        // Create builder notification, then create the content.
                        NotificationCompat.Builder buildNotification = new NotificationCompat.Builder(
                                StationService.this, "NotificationStationId"
                        );
                        buildNotification.setContentTitle(
                                "Brand New Activity on your Station: " + newStation.getName()
                        );
                        buildNotification.setContentText(
                                "Available bikes: " + newBikesAvailable + " ; Available docks " + newDocksAvailable
                        );
                        buildNotification.setSmallIcon(R.drawable.ic_launcher_background);
                        buildNotification.setAutoCancel(true);

                        // Now we must notify the user.
                        NotificationManagerCompat manageNotification = NotificationManagerCompat.from(StationService.this);
                        if (ActivityCompat.checkSelfPermission(StationService.this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        manageNotification.notify(1, buildNotification.build());

                        bikesAvailable = newBikesAvailable;
                        docksAvailable = newDocksAvailable;
                    }

                    // Waiting 30 secondes to avoid to overload API for nothing.
                    Log.d(TAG, "Every 30 secondes I will appear");
                    Thread.sleep(30000);

                } catch (InterruptedException ex) {
                    System.out.println(ex);
                    Log.d(TAG, "Thread interrupted");
                    return;
                }
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create channel notification
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "NotificationStationId",
                    "Notification Station Service",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        String subValidator = intent.getStringExtra("subscribe");
        bikesAvailable = intent.getStringExtra("bikesAvailable");
        stationId = intent.getStringExtra("stationId");
        docksAvailable = intent.getStringExtra("docksAvailable");

        Log.d(TAG, "onStartCommand: " + subValidator);
        Log.d(TAG, "bikesAvailable: " + bikesAvailable);
        Log.d(TAG, "stationId: " + stationId);
        Log.d(TAG, "docksAvailable: " + docksAvailable);

        if (subValidator.equals(new String("SUB"))){
            if (service == null){
                service = new subscribeStationService();
                service.start();
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        service.interrupt();
        service = null;
        Log.d(TAG, "onDestroy done.");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}