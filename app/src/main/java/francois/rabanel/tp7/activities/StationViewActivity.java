package francois.rabanel.tp7.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;

import francois.rabanel.tp7.R;
import francois.rabanel.tp7.api.Station;
import francois.rabanel.tp7.services.StationService;

public class StationViewActivity extends AppCompatActivity {
    String TAG = "StationViewActivity";
    String name, bikesAvailable, docksAvailable, stationId;
    TextView bikesAvailableElement, docksAvailableElement, nameElement;
    Button subscribeElement, unsubscribeElement;
    Intent intentService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_view);
        intentService = new Intent(StationViewActivity.this, StationService.class);

        // On récupère les données provenant de l'activité principale
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        bikesAvailable = intent.getStringExtra("bikesAvailable");
        docksAvailable = intent.getStringExtra("docksAvailable");
        stationId = intent.getStringExtra("stationId");

        // Puis nos éléments qu'on va mettre à jours
        bikesAvailableElement = (TextView) findViewById(R.id.available_bikes_value);
        docksAvailableElement = (TextView) findViewById(R.id.available_docks_value);
        nameElement = (TextView) findViewById(R.id.name);
        subscribeElement = (Button) findViewById(R.id.subscribe);
        unsubscribeElement = (Button) findViewById(R.id.unsubscribe);

        bikesAvailableElement.setText(bikesAvailable);
        docksAvailableElement.setText(docksAvailable);
        nameElement.setText(name);

        subscribeElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Abonnement SERVICE (interne) en lui fournissant station ID
                intentService.putExtra("bikesAvailable", bikesAvailable);
                intentService.putExtra("docksAvailable", docksAvailable);
                intentService.putExtra("stationId", stationId);
                intentService.putExtra("subscribe", new String("SUB"));

                startService(intentService);
                unsubscribeElement.setEnabled(true);
                subscribeElement.setEnabled(false);
            }
        });

        unsubscribeElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentService.removeExtra("subscribe");
                intentService.putExtra("subscribe", new String("UNSUB"));
                stopService(intentService);
                subscribeElement.setEnabled(true);
                unsubscribeElement.setEnabled(false);
            }
        });
    }
}