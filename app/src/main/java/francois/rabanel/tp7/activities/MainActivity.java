package francois.rabanel.tp7.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import francois.rabanel.tp7.R;
import francois.rabanel.tp7.adapters.VelibAdapter;
import francois.rabanel.tp7.api.HttpHandler;
import francois.rabanel.tp7.api.Station;

public class MainActivity extends AppCompatActivity {
    static String TAG = "MainActivity";
    Button LoadButtonElement, mapButton;
    ListView listVelibElement;
    public static HashMap<String, Station> hmapStations;
    static {
        hmapStations = new HashMap<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LoadButtonElement = (Button) findViewById(R.id.load_button);
        mapButton = (Button) findViewById(R.id.map_button);
        listVelibElement = (ListView) findViewById(R.id.list_velib);

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, 100);
        }

        LoadButtonElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoadButtonElement.setText("DATA LOADING...");

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Loading datas....");
                        loadStations(hmapStations);
                        loadCapacity(hmapStations);
                        Log.d(TAG, "Datas loaded.");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                LoadButtonElement.setText("REFRESH");
                                mapButton.setEnabled(true);
                                VelibAdapter adapter = new VelibAdapter(hmapStations);
                                listVelibElement.setAdapter(adapter);
                            }
                        });
                    }
                });
                thread.start();
            }
        });

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);

                // Je suis parti sur un JSON car le HashMap hmapStations est trop large pour être passé
                // dans un Intent.
                JSONArray jsonStations = null;
                try {
                    jsonStations = buildNewHashMapFromStations(hmapStations);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                intent.putExtra("jsonArray", jsonStations.toString());
                startActivity(intent);
            }
        });
    }

    private JSONArray buildNewHashMapFromStations(HashMap<String, Station> hmapStations) throws JSONException {
        // hmapStations <String, Station> = {"station_id": Station}
        // array [{...}, {...}, {...}, ...]
        JSONArray array = new JSONArray();

        for (Station value : hmapStations.values()) {
            JSONObject dict = new JSONObject();
            dict.put("name", value.getName());
            dict.put("longitude", value.getLon());
            dict.put("latitude", value.getLat());
            dict.put("bikes", value.getNumBikesAvailable());
            array.put(dict);
        }
        return array;
    }

    public static boolean loadStations(HashMap<String, Station> hmap_stations) {
        HttpHandler sh = new HttpHandler();

        // Making a request to url and getting response
        String url = "https://velib-metropole-opendata.smoove.pro/opendata/Velib_Metropole/station_information.json";
        String jsonStr = sh.makeServiceCall(url);
        Log.e(TAG, "Response from url: " + jsonStr);
        if (jsonStr == null) {
            Log.e(TAG, "Couldn't get json from server.");
            return false;
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONObject j_data = jsonObj.getJSONObject("data");
            JSONArray stations = j_data.getJSONArray("stations");
            // looping through All stations
            for (int i = 0; i < stations.length(); i++) {
                JSONObject c = stations.getJSONObject(i);
                String station_id = c.getString("station_id");
                String name = c.getString("name");
                String lat = c.getString("lat");
                String lon = c.getString("lon");
                String capacity = c.getString("capacity");
                String stationCode = c.getString("stationCode");
                Log.i(TAG, "Json station item: " + station_id + " - "+name+" - "+lat+" - "+lon+" - "+capacity+" - " + stationCode);
                Station s = new Station(station_id, name, lat, lon, capacity, stationCode);
                hmap_stations.put(station_id, s);
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
        }
        return true;
    }
    public static boolean loadCapacity(HashMap<String, Station> hmap_stations) {
        HttpHandler sh = new HttpHandler();

        // Making a request to url and getting response
        String url = "https://velib-metropole-opendata.smoove.pro/opendata/Velib_Metropole/station_status.json";
        String jsonStr = sh.makeServiceCall(url);
        Log.e(TAG, "Response from url: " + jsonStr);
        if (jsonStr == null) {
            Log.e(TAG, "Couldn't get json from server.");
            return false;
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONObject j_data = jsonObj.getJSONObject("data");
            JSONArray stations = j_data.getJSONArray("stations");

            // looping through All stations
            for (int i = 0; i < stations.length(); i++) {
                JSONObject c = stations.getJSONObject(i);
                String station_id = c.getString("station_id");
                String numBikesAvailable = c.getString("numBikesAvailable");
                String numDocksAvailable = c.getString("numDocksAvailable");
                Station s =(Station)hmap_stations.get(station_id);
                if(s!=null) {
                    s.setNumBikesAvailable(numBikesAvailable);
                    s.setNumDocksAvailable(numDocksAvailable);
                    Log.i(TAG, "Json station item: " + station_id + " - "+numBikesAvailable+" - " + numDocksAvailable);
                } else {
                    Log.i(TAG, "Station Id not found : "+station_id);
                }
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
        }
        return true;
    }
}