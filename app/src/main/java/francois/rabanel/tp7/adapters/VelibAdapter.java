package francois.rabanel.tp7.adapters;

import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import francois.rabanel.tp7.R;
import francois.rabanel.tp7.activities.StationViewActivity;
import francois.rabanel.tp7.api.Station;

public class VelibAdapter extends BaseAdapter {
    private final ArrayList mData;
    public static HashMap<String, Station> hmapStations;
    String TAG = "VelibAdapter";

    public VelibAdapter(HashMap<String, Station> stations) {
        mData = new ArrayList();
        mData.addAll(stations.entrySet());
        hmapStations = stations;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Map.Entry<String, String> getItem(int position) {
        return (Map.Entry) mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO implement you own logic with ID
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View result;

        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.velib_items, parent, false);
        } else {
            result = convertView;
        }

        Map.Entry<String, String> item = getItem(position);

        // On récupère l'objet Station via le station_id qui est dans item.
        String station_id = item.getKey();
        Station s = hmapStations.get(station_id);
        String bikesAvailable = s.getNumBikesAvailable();
        String stationName = s.getName();

        ((TextView) result.findViewById(R.id.station_name)).setText(stationName);
        TextView capacityElement = (TextView) result.findViewById(R.id.capacity);

        if (bikesAvailable.equals(new String("0"))) {
            capacityElement.setText(bikesAvailable);
            capacityElement.setTextColor(Color.RED);
        } else {
            capacityElement.setText(bikesAvailable);
            capacityElement.setTextColor(Color.GREEN);
        }
        
        result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentToStationViewActivity = new Intent(
                        view.getContext(), StationViewActivity.class
                );
                intentToStationViewActivity.putExtra("name", stationName);
                intentToStationViewActivity.putExtra("bikesAvailable", bikesAvailable);
                intentToStationViewActivity.putExtra("docksAvailable", s.getNumDocksAvailable());
                intentToStationViewActivity.putExtra("stationId", s.getStation_id());
                view.getContext().startActivity(intentToStationViewActivity);
            }
        });
        
        
        return result;
    }
}

